#include "gui/gui.cpp"
#include "animation/animation.cpp"
#include "igraph/_igraph.cpp"
#include "spacy/spacy.cpp"
#include "filehandler/filehandler.hpp"
#include "strings/strings.hpp"
#include "Logger.h"

#include <iostream>
#include <fmt/core.h>
#include <vector>
#include <string>
#include <filesystem>

namespace ig = _igraph;
namespace fh = fileHandler;

int main() {
    auto folder = std::filesystem::path("../data/All_FT");

    LogInfo << "Creating the spacy object" << std::endl;

    auto Spacy_Obj = _spacy("NLP_Legal_Data");

    LogInfo << "Reading the data" << std::endl;

    unsigned count {};

    for (const std::filesystem::directory_entry& v :
            std::filesystem::directory_iterator{folder}) {

       auto file = fh::FileHandler(v.path().c_str());

       LogInfo << "Handling file name : " << v << std::endl;
       LogInfo << "Total files handled : " << ++count << std::endl;
       std::string data = file.GetAllLines();
       std::vector<std::string> parsed_data {stringhandler::parse_data(data)};
       std::vector<std::string> metadata {stringhandler::parse_metadata(parsed_data[0])};
       Spacy_Obj.add_docstring(parsed_data[1]);
       stringhandler::remove_pronouns_from_batch(Spacy_Obj.get_noun_phrases_self()[Spacy_Obj.get_noun_phrases_self().size() - 1]);
    }

    LogInfo << "Finished reading the data" << std::endl;
}
