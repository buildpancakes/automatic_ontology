#ifndef SPACY_H_
#define SPACY_H_

#include <string>
#include <spacy/spacy>
#include <spacy/spacy.h>
#include <vector>

const char* version = "4.0";

Spacy::Spacy spacy;

class _spacy {
    public:
        _spacy(const std::string&);

        void initialize(const std::string&);

        // Accessors
        std::vector<Spacy::Doc> get_doclist() {return doc_list;};
        Spacy::Nlp get_nlp() {return nlp;};
        std::vector<std::string> get_noun_phrases(const Spacy::Doc &);
        std::vector<std::vector<std::string>> get_noun_phrases_self();

        // Setters
        void set_nlp(); // Not sure how to set this one
        void add_docstring(const std::string&);

        // Printing
        void print_doc(const Spacy::Doc&);
        void print_doc_self();
        void print_noun_phrases(const Spacy::Doc& doc);
        void print_noun_phrases_self();

        /*
        ** @brief Cleans noun chunks
        ** @details Clean the noun chunks using the following pattern
        ** 1. Keyword Elimination of most used words
        ** 2. Removing Names
        */

    private:
        std::string name {};
        Spacy::Nlp nlp = spacy.load("en_core_web_trf");
        // TODO Maybe multithread this to get better inputting speed
        std::vector<Spacy::Doc> doc_list {};

        // Is this going to be compile time tho
        // Maybe there is a good reason to keep this interpreted

        // Spacy Language object

};

/*
** auto nlp = spacy.load("en_core_web_sm");
** auto doc = nlp.parse("This is a sentence.");
** for (auto& token : doc.tokens())
**   std::cout << token.text() << " [" << token.pos_() << "]\n";
*/

#endif // SPACY_H_
