#include "spacy.hpp"
#include <fmt/core.h>

_spacy::_spacy(const std::string& name) {
    _spacy::initialize(name);
};

void _spacy::initialize(const std::string& name){
    this->name = name;
    // Initialize a single doc for check and delete it
    doc_list.push_back(nlp.parse("This is some text"));
    doc_list.clear();
}

void _spacy::print_doc(const Spacy::Doc& doc) {
    for (const auto& token : doc.tokens()) {
        fmt::print("{} - [{}]\n", token.text(), token.pos_());
    }
}

void _spacy::print_noun_phrases(const Spacy::Doc& doc) {
    for (const auto& token : doc.noun_chunks()) {
        fmt::print("{} - [LABEL/{}] - [LEMMA/{}] - [VN/{}]\n",
                   token.text(), token.label_(),
                   token.lemma_(), token.vector_norm());
    };
}

void _spacy::print_doc_self() {
    for (const Spacy::Doc& doc : this->doc_list)
        _spacy::print_doc(doc);
}

void _spacy::print_noun_phrases_self() {
    for (const Spacy::Doc& doc : this->doc_list)
        _spacy::print_noun_phrases(doc);
}

void _spacy::add_docstring(const std::string & data){
    doc_list.push_back(this->nlp.parse(data));
}

std::vector<std::string> _spacy::get_noun_phrases(const Spacy::Doc & doc) {
    std::vector<std::string> otp_vec {};
    for (const Spacy::Span& token : doc.noun_chunks()) {
        otp_vec.push_back(token.text());
    }
    return otp_vec;
}

std::vector<std::vector<std::string>> _spacy::get_noun_phrases_self() {
    std::vector<std::vector<std::string>> otp_vec {{}};
    for (const Spacy::Doc& doc : this->doc_list) {
        otp_vec.push_back(get_noun_phrases(doc));
    }
    return otp_vec;
}
