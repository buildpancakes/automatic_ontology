#ifndef STRINGS_H_
#define STRINGS_H_

#include <vector>
#include <string>


namespace stringhandler {

    /*
    ** @brief Types of treatments done in cases
    ** @note Please check datafile xxxxxx for reference
    */
    enum JUDGEMENT_TYPE {
    NEGATIVE,
    NEUTRAL_OR_NEGATIVE,
    NEUTRAL,
    POSITIVE_OR_NEUTRAL,
    POSITIVE
    };

    /*
    ** @brief Party separator
    */
    const std::string party_separator {" v "};

    /*
    ** @brief Some terms to ignore in the data
    ** @note It might have been better to parse this part of the document separately
    */
    const std::vector<std::string> terms_to_ignore {
    "The Order of the Court was as follows",};

    /*
    ** @brief Judgment clauses to find for
     */
    const std::string judgement_clause {"The Judgment was delivered by"};

    /*
    ** @brief Judgment separator clause in file
     */
    const std::string judgement_clause_separator {":"};

    /*
    ** @brief Judgement separator we will use to identify separate judges in case
     */
    const std::string judge_separator {"|"};

    /*
    ** @brief Comment separator
     */
    const std::string comment_separator {"|"};


    /*
    ** Some of the number formats found in data
    ** - ^(\d*)
    ** - ^\d*.
    ** - ^\d*\
    ** NOTE THERE ARE NO INSTANCES OF 3 DIGITS SO WE CAN SAFELY CONSIDER 4 DIGITS FOR OUR SHENANIGANS
     */
    const std::vector<std::string> regex_number_prefix_patterns = {{
        R"(^(\d*))",
        R"(\^\d*.)",
        R"(\^\d*\s)"
    }};


    /*
    ** Some of the name formats found in data
    ** - \W.\W. \w*
    ** - \W
    ** - ^\d*\
    ** NOTE THERE ARE NO INSTANCES OF 3 DIGITS SO WE CAN SAFELY CONSIDER 4 DIGITS FOR OUR SHENANIGANS
     */
    const std::vector<std::string> regex_name_prefix_patterns = {{
        R"(\W.\s?\W.\s?\w{2,})", // R. L. Meena
    }};

    /*
    ** @brief All keywords deemed not worthy
     */

    /*
    ** @brief : Some of the articles that are non-necessary
     */
    const std::vector<std::string> personal_pronouns {{
         "i",
         "you",
         "he",
         "she",
         "it",
         "we",
         "they",
         "them",
         "us",
         "him",
         "her",
         "his",
         "hers",
         "its",
         "theirs",
         "our",
         "your",
    }};


    // TODO Not make this custom parse
    std::string remove_number_prefix (const std::string&);

    /*
    ** @brief separates all judges based on and clause
     */
    std::vector<std::string> separate_judges (const std::string&);


    /*
    ** @brief Parses the whole data and in case it does not throws an error
    ** @details
    ** This will give the following content in the mentioned manner
    ** 1. Metadata
    ** 2. Proceedings/Points of the court
    ** 3. Judgement / Something actually done by the whole thing
    */
    std::vector<std::string> parse_data(const std::string&);

    /*
    ** @brief Parse contents of metadata
    ** @details
    ** Metadata will be parsed into these categories
    ** 1. Appellant ( I understand this is the legal term )
    ** 2. Defendant ( I am sorry if this term is incorrect but lets work with it)
    ** 3. Location of Judgement ( Usually Supreme Court )
    ** 4. Date of Ruling (Usually present)
    ** 5. Judge ruling (someitmes this might be empty)
    ** // This might be positive neutral negative
    ** 6. Writs and Appeals (this is based on; Longer string)
    ** 7. Comments ( These are extra stuff if found on the metadata )
     */
    std::vector<std::string> parse_metadata(const std::string&);

    /*
    ** @brief Stores all the contents
     */
    const std::vector<std::string> metadata_contents{{
        "Appellant",
        "Defendant",
        "Location of Judgement",
        "Date of Ruling",
        "Judge Ruling",
        "Writs and Appeals",
        "Comments"
    }};

    /*
    ** @brief Prints out all the metadata
     */
    void print_metadata(const std::vector<std::string>&);

    /*
    ** @breif Trims the whitespaces in front and back
     */
    std::string trim_whitespace(const std::string&);

    /*
    ** @brief To lowercase
     */
    std::string to_lowercase(const std::string&);

    /*
    ** @brief Trims the first instance of number happening
     */
    std::string trim_prefix_numbers(const std::string&);

    /*
    ** @brief Convert time to a better standard format
     */
    std::string time_better_format(const std::string&);

    /*
    ** @brief Separate newlines
     */
    std::vector<std::string> separate_newlines(const std::string&);

    /*
    ** @brief Recognizes names
     */
    bool is_name (const std::string&);

    /*
    ** @brief Remove personal pronouns
    ** TODO Might not need it
     */
    std::string remove_pronouns(const std::string &);

    /*
    ** @brief Remove pronouns from batch of phrases
     */
    std::vector<std::string> remove_pronouns_from_batch(const std::vector<std::string> &);

    /*
    ** @brief Just a weird printing module
     */
    void print_batch_of_words(const std::vector<std::string>&, const std::string&);

    /*
    ** @brief Remove stopwords
     */
    std::string remove_stop_words(const std::string&,
                                  const std::vector<std::string>&);
}

#endif // STRINGS_H_
