project(StringHandler C CXX)

set(SOURCE_FILES
    strings.hpp
    strings.cpp
)


add_library(StringHandler STATIC ${SOURCE_FILES})
add_compile_options(-Wall -Wextra -pedantic -Wextra -O5)

install(TARGETS StringHandler DESTINATION ${AO_INSTALL_LIB_DIR})
install(FILES strings.hpp DESTINATION ${AO_INSTALL_INCLUDE_DIR})
