/*
** This file is just to capture around the type of data I am handling
** Please do not / refer it
** I had to do manual data manipualtion to just fit the things into this type of data
*/

#include "strings.hpp"

// DEBUG DELETE WHEN NOT REQUIRED
#include <fmt/core.h>

#include <string>
#include <cctype>
#include <chrono>
#include <iomanip>
#include <locale>
#include <sstream>
#include <vector>


namespace stringhandler{


    std::vector<std::string> parse_data(const std::string& data) {
        // Parse the metadata, proceeding, final judgement
        // Lets get the metadata by using the single number
        // Can we use a regex parse
        size_t proceedings_final_judgemenet {};
        size_t metadata_proceedings = data.find("\n1. ");
        bool is_final_judgement_present {false};

        size_t last_line_pos = data.substr(0, data.rfind("\n")).rfind("\n");
        if (!isdigit(data.substr(last_line_pos+1)[0])) {
            is_final_judgement_present = true;
        }

        // fmt::print("DATA SIZE : {}\n", data.substr(data.substr(0, data.rfind("\n")).rfind("\n")));
        // fmt::print("DATA SIZE : {}\n", data.substr(last_line_pos));
        // fmt::print("LAST LINE : {}\n",last_line_pos);

        // fmt::print("CHECK : {}\n", (data.substr(last_line_pos+1)[0]));

        std::string metadata {};
        std::string proceedings {};
        std::string final_judgement {};

        if (metadata_proceedings != std::string::npos) {
            metadata = data.substr(0, metadata_proceedings);
        }

        if (is_final_judgement_present) {
            proceedings = data.substr(metadata_proceedings+1, last_line_pos - metadata_proceedings - 2);
            final_judgement = data.substr(last_line_pos + 1, data.size() - last_line_pos - 2);
        } else {
            proceedings = data.substr(metadata_proceedings+1, data.size() - metadata_proceedings - 2);
        }

        return std::vector<std::string> {metadata, proceedings, final_judgement};
    }

    std::vector<std::string> parse_metadata(const std::string& data) {

        // Let's go with each one, one by one
        // Maybe even include the type of case we are handling
        // TODO Judgement / Petition / Appeal
        std::string appellant    {};
        std::string defendent    {};
        std::string location     {};
        std::string date         {};
        std::string judge        {};
        std::string writs_appeals{};
        std::string comments     {};

        // We need to keep a zero check as size_t cant be negatively intiaized
        size_t idx {};
        size_t prev_idx {};

        // This is a bother to solve for
        std::vector<std::string> lines {separate_newlines(data)};


        // for (const auto& v : lines)
        //     fmt::print("LINES : {}\n", v);

        // Start with parties
        // First line
        size_t v_instance {lines[0].find(party_separator)};
        if (v_instance == std::string::npos)
            // THESE ARE THOSE RE FILES
            return std::vector<std::string> {party_separator, defendent, location, judge, writs_appeals};
        appellant = trim_whitespace(lines[0].substr(0, v_instance));
        defendent = trim_whitespace(lines[0].substr(v_instance + party_separator.size()));

        // Second line
        // There are two variants
        // See : Formats discussions on README.org
        size_t current_line {1};
        if (lines[current_line].find("Supreme Court of India") != std::string::npos) {
            location = "Supreme Court of India";
            ++current_line;
        }
        // This is just for those 119 files
        else {
            if (lines[current_line].find("Status:") == std::string::npos)
                exit(1001);
            current_line++;

            // TODO Might need to categorize these too
            // The order matters here as there is a "neutral" and "positive or neutral"
            if (lines[current_line].find("Positive Judicial Treatment") != std::string::npos) {
                comments += "POSITIVE JUDGEMENT RULED" + comment_separator;
            }
            else if (lines[current_line].find("Positive or Neutral Judicial Treatment") != std::string::npos){
                comments += "POSITIVE OR NEUTRAL JUDGEMENT RULED" + comment_separator;
            }
            else if (lines[current_line].find("Neutral or Negative Judicial Treatment") != std::string::npos){
                comments += "NEUTRAL OR NEGATIVE JUDGEMENT RULED" + comment_separator;
            }
            else if (lines[current_line].find("Neutral Judicial Treatment") != std::string::npos){
                comments += "NEUTRAL JUDGEMENT RULED" + comment_separator;
            }
            else if (lines[current_line].find("Negative Judicial Treatment") != std::string::npos){
                comments += "NEGATIVE JUDGEMENT RULED" + comment_separator;
            }
            ++current_line;
            location = "Supreme Court of India";
            ++current_line;
        }

        // 3rd part is now whitespace since in the data the parsed data
        ++current_line;


        // 4th part is now dates (Checked and verified)
        date = lines[current_line];
        current_line++;

        // Most end there so there should be a check
        // If not then there are these categories
        // 1. Writs and Appeals
        // 2. Judgement
        for (; current_line <= lines.size() - 1; ++current_line){

            // fmt::print("LINE_ : {}\n", current_line);

            std::string line {lines[current_line]};
            if (line.size() == 0) continue;

            // Early checking for terms to ignore
            bool ignore_this_line {false};
            for (const auto &v : terms_to_ignore ) {
                if (line.find(v) != std::string::npos){
                    ignore_this_line = true;
                    break;
                }
            }
            if (ignore_this_line) continue;

            // Judges
            if (lines[current_line].find(judgement_clause) != std::string::npos) {
                size_t judgement_pos {lines[current_line].find(judgement_clause)};
                std::string judges_clause = lines[current_line].substr(judgement_pos + judgement_clause.size());
                judges_clause = judges_clause.substr(judges_clause.find(judgement_clause_separator) + judgement_clause_separator.size());
                std::vector<std::string> judges {separate_judges(judges_clause)};
                for (const auto& _judge : judges) {
                    judge += _judge + judge_separator;
                }
            } else {
                writs_appeals += lines[current_line];
            }
        }

        return std::vector<std::string> {appellant, defendent, location, date, judge, writs_appeals, comments};

    }

    std::string trim_whitespace(const std::string& text) {
        size_t _front_idx {};
        size_t _back_idx  {text.size()-1};

        while (isspace(text[_front_idx])) _front_idx++;
        while (isspace(text[_back_idx]))  _back_idx-- ;

        return text.substr(_front_idx, _back_idx - _front_idx + 1);
    }

    std::vector<std::string> separate_judges(const std::string& data) {

        // TODO Maybe have a parser to remove the honorifics
        // Capitalize them anyway

        std::vector<std::string> judges{};

        size_t and_pos {};
        size_t prev_pos {};

        std::string term {" and "};
        while(and_pos = data.find(term, prev_pos), and_pos != std::string::npos){
            judges.push_back(trim_whitespace(
                                 data.substr(prev_pos, and_pos - prev_pos)));
            prev_pos = and_pos + term.size();
        }
        judges.push_back(data.substr(prev_pos));

        return judges;
    }

    void print_metadata(const std::vector<std::string>& data) {
        // TODO Maybe a cleaner interface for printing stuff out
        if (data.size() != metadata_contents.size()) {
            fmt::print("Something wrong with the data\n");
        }
        for (int i = 0 ; i < data.size(); ++i) {
            fmt::print("{} : {}\n", metadata_contents[i], data[i]);
        }
    }

    std::vector<std::string> separate_newlines(const std::string& data) {
        size_t prev_idx{};
        size_t idx {};

        std::vector<std::string> lines {};

        while (idx = data.find('\n',prev_idx+1), idx != std::string::npos) {
            if (prev_idx == 0)
                lines.push_back(trim_whitespace(data.substr(prev_idx, idx - (prev_idx+1))));
            else
                lines.push_back(trim_whitespace(data.substr(prev_idx+1, idx - (prev_idx+1))));
            prev_idx = idx;
        }

        // Last one
        lines.push_back(trim_whitespace(data.substr(prev_idx+1)));

        return lines;
    }

    // TODO Trim the prefix numbers
    std::string trim_prefix_numbers(const std::string& data) {
        std::vector<std::string> lines {separate_newlines(data)};

        std::string otp_data {};

        for (const auto& line : lines) {
        }

        return data;
    }

    // TODO To implement this
    std::string remove_number_prefix ( const std::string& data ) {
        if (data.size() != 4) {
            fmt::print("INCORRECT FORMAT\n");
            return data;
        }

        std::string copy {data};
        if (isalpha(data[3]) or isspace(data[3])) copy = data.substr(0,3);
        return data;
    }


    bool is_name(const std::string& data){
        // R.L. Meena

        return false;
    };

    std::string remove_pronouns(const std::string & data) {
        std::string lowercase {to_lowercase(data)};
        std::string output {data};
        for (const std::string& pronoun : personal_pronouns) {
            size_t idx {};

            // NOTE Preliminary case if pronoun = phrase
            if (lowercase == pronoun or lowercase == pronoun + " ")
                return "";

            //First case
            if (lowercase.substr(0,pronoun.size()).find(pronoun+" ") != std::string::npos) {
                output = output.replace(0, pronoun.size() + 1, "");
                lowercase = to_lowercase(output);
            }

            // Later cases
            while (idx = lowercase.find(" " + pronoun + " "), idx != std::string::npos) {
                output = output.replace(idx, pronoun.size() + 1, "");
                lowercase = to_lowercase(output);
            }
        }
        return output;
    };

    std::string to_lowercase(const std::string & data) {
        std::string otp {};
        for (const char& chr : data) {
            otp += tolower(chr);
        }
        return otp;
    }

    std::vector<std::string> remove_pronouns_from_batch
        (const std::vector<std::string>& phrase_vector) {

        std::vector<std::string> vec {};
        for (const std::string& phrase : phrase_vector) {
            std::string after_pronoun_removal {remove_pronouns(phrase)};
            if (!after_pronoun_removal.size()) {
                continue;
            }
            vec.push_back(after_pronoun_removal);
        }
        return vec;
    }

    void print_batch_of_words(const std::vector<std::string>& data,
                              const std::string& param = "WORD"){
        for (const std::string& v : data) {
            fmt::print("{} - {}\n", param, v);
        }
    }
}
