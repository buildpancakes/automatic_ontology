project(Igraph C CXX)

set(SOURCE_FILES
    _igraph.hpp
    _igraph.cpp
)

add_library(Igraph SHARED STATIC ${SOURCE_FILES})

message("Checking if igraph library is present")
message("In case error occurs here please install https://igraph.org/")

find_package(igraph REQUIRED)
# include_directories(${igraph_INCLUDE_DIRS})
target_link_libraries(Igraph PUBLIC igraph::igraph)

install(TARGETS Igraph DESTINATION ${AO_INSTALL_LIB_DIR})
install(FILES igraph.h DESTINATION ${AO_INSTALL_INCLUDE_DIR})
