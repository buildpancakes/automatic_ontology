#!/usr/bin/env python3

from nb_section_extract import timeout
import re
import os
import tqdm
import ast

def add_to_count_dict(term : str, dictionary : dict) -> None:
    if term in dictionary.keys():
        dictionary[term] += 1
    else:
        dictionary[term] = 0


@timeout(10)
def check_ipc(data:str) -> bool:
    ipc_str = "(Indian Penal Code|[Ii]\\.?[Pp]\\.?[Cc]\\.?)"
    ipc = re.search(ipc_str, data)
    if (ipc == None): return False
    else : return True


if __name__ == "__main__":

    timed_out_files             : list[str] = []
    other_except                : list[tuple[str, str]] = []
    files_with_no_ipc           : list[str] = []
    files_with_ipc_but_no_ipc   : list[str] = []
    files_with_ipc              : list[str] = []
    files_with_ipc_not_process  : list[str] = []
    files_with_ipc_but_no_exist : list[str] = []
    files_with_no_ipc_no_exist  : list[str] = []

    ipc_count_dict                      : dict = dict()
    ipc_count_dict_spacy                : dict = dict()
    non_ipc_count_dict                  : dict = dict()
    non_ipc_count_dict_spacy            : dict = dict()

    extracted_data : str = "../.extracted_data/"
    data_folder : str = "../.data2/"

    files : list[str] = os.walk(data_folder).__next__()[2]
    for file in tqdm.tqdm(files):
        try:
            with open(data_folder + file) as fp:
                data : str = "".join(fp.readlines()[6:])
            if (check_ipc(data)):
                files_with_ipc.append(file)
            else :
                files_with_no_ipc.append(file)

        except TimeoutError:
            timed_out_files.append(file)
        except Exception as e:
            other_except.append((file, e.__str__()))

    for file in tqdm.tqdm(files_with_ipc):

        if (os.path.exists(extracted_data + file)):

            with open(extracted_data + file, 'r') as fp:
                data : str = fp.readlines()

            if (len(data) <= 10):
                files_with_ipc_not_process.append(file)
            elif (data[11] == "[]\n"):
                files_with_ipc_but_no_ipc.append(file)

            words       : dict = ast.literal_eval(data[7][:-1])
            words_spacy : dict = ast.literal_eval(data[9][:-1])


            for word in words.keys():
                add_to_count_dict(word, ipc_count_dict)

            for word in words_spacy.keys():
                add_to_count_dict(word, ipc_count_dict_spacy)

        else :
            files_with_ipc_but_no_exist.append(file)

    for file in tqdm.tqdm(files_with_no_ipc):

        if (os.path.exists(extracted_data + file)):
            with open(extracted_data + file, 'r') as fp:
                data : str = fp.readlines()

            if (len(data) <= 10):
                files_with_ipc_not_process.append(file)
            elif (data[11] == "[]\n"):
                files_with_ipc_but_no_ipc.append(file)

            words       : dict = ast.literal_eval(data[7][:-1])
            words_spacy : dict = ast.literal_eval(data[9][:-1])


            for word in words.keys():
                add_to_count_dict(word, non_ipc_count_dict)

            for word in words_spacy.keys():
                add_to_count_dict(word, non_ipc_count_dict_spacy)

        else :
            files_with_no_ipc_no_exist.append(file)

    final_dict : dict[str, float] = dict()
    print("Processing final dictionary")

    # 1 - smoothing
    for word in ipc_count_dict.keys():
        if word in non_ipc_count_dict.keys():
            final_dict[word] = (ipc_count_dict[word] + 1) / (non_ipc_count_dict[word] + 1)
        else :
            final_dict[word] = (ipc_count_dict[word] + 1) / 1


    final_count = dict(sorted(final_dict.items(),
                          key = lambda x : x[1], reverse=True))

    final_dict_spacy : dict[str, float] = dict()

    for word in ipc_count_dict_spacy.keys():
        if word in non_ipc_count_dict_spacy.keys():
            final_dict_spacy[word] = (ipc_count_dict_spacy[word] + 1) / (non_ipc_count_dict_spacy[word] + 1)
        else :
            final_dict_spacy[word] = (ipc_count_dict_spacy[word] + 1) / 1

    final_count_spacy = dict(sorted(final_dict_spacy.items(),
                          key = lambda x : x[1], reverse=True))

    print("Saving final dictionary")

    with open('../notes/ipc_ratio.log', 'w') as fp:
        for word in final_count.keys():
            if word in non_ipc_count_dict.keys():
                fp.write(f"{word} => {final_count[word]} => {non_ipc_count_dict[word]}\n")
            else :
                fp.write(f"{word} => {final_count[word]} => 0\n")

    with open('../notes/ipc_ratio_spacy.log', 'w') as fp:
        for word in final_count_spacy.keys():
            if word in non_ipc_count_dict_spacy.keys():
                fp.write(f"{word} => {final_count_spacy[word]} => {non_ipc_count_dict_spacy[word]}\n")
            else :
                fp.write(f"{word} => {final_count_spacy[word]} => 0\n")
