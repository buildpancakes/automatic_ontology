#!/usr/bin/env python3

import os
import re
import string
import nb_nlp
import tqdm
import time
import errno
import signal
import functools

class TimeoutError(Exception):
    pass

def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wrapper

    return decorator

@timeout(10)
def section_extract(data : str) -> list[str]:
    sections : list[str] = list()


    starting = "(ss\\.\\s?|[sS]\\.\\s?|[Ss]ections?,?\\s?|u/s\\.\\s?|es\\.\\s?|Sec\\.\\s?)"
    section_style = "[0-9]{1,3}-?([A-Za-z])?"
    spacing = ".{0,100}"
    ipc = "(Indian Penal Code|[Ii]\\.?[Pp]\\.?[Cc]\\.?)"

    pattern : str = rf'\b{starting}(({section_style}/)*({section_style})*,? )*(and |or )?(({section_style}/)*({section_style})){spacing} {ipc}'
    # \b was required as the pattern also accepted Rs. 100 formats

    sections = re.findall(pattern, data)

    unique_sections: list[str] = []

    # Maybe a better idea is to create a reference list to refer to realtime to
    # check if the section is valid or not

    for section in sections:
        for element in section:
            if (element.find('-') > -1):
                element = "".join(element.split('-'))
            if (len(element)>0 and (element[0] in string.digits)):
                if (element.find('/') == len(element) - 1): # There is a weird group formed out of the regex "420B/"
                    unique_sections.append(element[:-1])
                elif (element.find('/') > -1 or element.find(',') > -1):
                    continue
                else:
                    if element not in unique_sections:
                        unique_sections.append(element)

    return unique_sections

def test_section_extract() -> list[str]:
    txt  = ""
    return section_extract(txt)


if __name__ == "__main__":

    # u/s. 120
    # S. 120B, IPC,
    # under ss. 380, 467 and 900, I.P.C.
    # S. 471
    # S. 420 read with S. 511, IPC.
    # ss. 467, 420, 420 read with 511 and 471, I.P.C.
    # ss. 380 and 467 I.P.C.,
    # ss. 467, 471, 420 and 419 I.P.C.
    # ss. 380, 467/471, 420/511 read with S. 120B I.P.C. ss. 467, 471 and 420 S. 120B IPC.ss. 380 and 467 read with 120-B, IPC.
    # S. 196A of ss. 467 and 471 S. 120B IPC; under S. 196A .
    # ss. 467I/471J read with S. 120B, IPC S. 196A S. 420 IPC,
    # S. 420 IPC. under S. 196A
    # s. 120B, es. 466 and 467

    # This pattern should be different according to the states involved and data
    # For my use case, we are using the indian legal system and hence the sections
    # do not go beyond A-I but we are taking A-Z
    # The above comment should have strings extracted from some documents as a test example

    # ========== FILE HANDLING ====================

    folder_str = "../.data2/"
    to_keep_data = "../.extracted_data/"
    folder = nb_nlp.Folder(folder_str)
    files = folder.get_files()
    # print(files)
    # files = ['2004_G_39.txt', '2009_M_138.txt', '2001_S_1140.txt']
    error_files : list[tuple[str, str]] = list()
    weird_files : list[str] = list()
    non_exist_files : list[str] = list()
    timed_out_files : list[str] = list()
    ipc = "(Indian Penal Code|[Ii]\\.?[Pp]\\.?[Cc]\\.?)"

    for file_index in tqdm.tqdm(range(len(files))):
    # for file_index in range(len(files)):
        tmp_file = folder.File(folder.folder_path, files[file_index])
        tmp_file.read_file()
        try:
            tmp_file.parse_file()
            if (os.path.exists(to_keep_data + files[file_index]) and
                    len(re.findall(ipc, tmp_file.main_data)) > 0):
                # print(files[file_index])
                # print(re.findall(ipc, tmp_file.main_data))
                # print(tmp_file.main_data)
                semain_data = section_extract(tmp_file.main_data)
                with open(to_keep_data + files[file_index], 'a+') as fp:
                    fp.write("\n{}\n".format(semain_data))
            elif (os.path.exists(to_keep_data + files[file_index])):
                weird_files.append(files[file_index])
                with open(to_keep_data + files[file_index], 'a+') as fp:
                    fp.write("\n{}\n".format(list()))
            else:
                non_exist_files.append(files[file_index])

        except TimeoutError :
            timed_out_files.append(files[file_index])
        except Exception as e:
            error_files.append((files[file_index], e.__str__()))

    print(f"ERROR files : {error_files}")
    print(f"WEIRD files : {weird_files}")
    print(f"TIMED files :{timed_out_files}")
    print(f"NON_EXTIST files : {non_exist_files}")
