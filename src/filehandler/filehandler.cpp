#include "filehandler.hpp"

#include <fstream>
#include <string>
#include <fmt/core.h>
#include <filesystem>

namespace fileHandler {

  FileHandler::FileHandler() : _filename {} {}

  FileHandler::FileHandler(const std::string & filename) : _filename {filename} {
    _file.open(_filename, _file.in);
    if (!_file.is_open()) throw FILE_ERROR_OPENING;
    //no_of_lines = std::count(std::istreambuf_iterator<char>(_file),
    //           std::istreambuf_iterator<char>(), '\n');
    current_line = 0;
  };

  std::string FileHandler::GetAllLines() {
    std::string data{};
    std::string part{};

    // Read a Line from File
    while(std::getline(_file, part))
        data += part + '\n';
    return data;
  }

  FileHandler::~FileHandler() {
    if (_file.is_open()) {_file.close();}
  };

}

namespace folderHandler {

  std::vector<std::string> get_file_list() {

    std::vector<std::string> list_of_files {};
    return list_of_files;
  };

}
